<?php

require __DIR__ . '/vendor/autoload.php';

use DeviceParser\Parser;
use DeviceLocator\LocatorFactory;
use DeviceLocator\Drivers\OpenCellID;
use DeviceLocator\Drivers\GoogleService;
use Distance\Range;

$hospitals = array(
	[
		'name' => "Betanija",
		'name_extended' => "Novi Sad",
		'clinic_nr' => '3',
		'longitude' => 19.829408,
		'latitude' => 45.258361,
		'street' => 'Branimira Ćosića',
		'street_nr' => '37',
		'place' => 'Novi Sad',
		'zip' => '21000',
		'radius' => 800,

	],
	[
		'name' => "Srbobransko porodiliste",
		'name_extended' => "Ambulanta",
		'clinic_nr' => '1',
		'longitude' => 19.785231,
		'latitude' => 45.548808,
		'street' => 'Jovana Popovića',
		'street_nr' => '1',
		'place' => 'Srbobran',
		'zip' => '21480',
		'radius' => 2000,

	]
);

function nameOfInRangeHospitals($event, $hospitals){

 	$nameOfHospitals="";

 	foreach ($hospitals as $hospital) {

 		if($event['event'] != 'BTS') continue;

 		try {

 			$position = ['lat' => $event['latitude'], 'lon' => $event['longitude']];
 			$destination = ['lat' => $hospital['latitude'] , 'lon' => $hospital['longitude']];



 			if 	(	$position['lat'] && $position['lon'] && $destination['lat'] && $destination['lon'] &&

 					Range::inRange($position, $destination, $hospital['radius']/1000 )
				){

 					$nameOfHospitals .= $hospital['name'] . " "
 									. round (Distance\DistanceFactory::calculate(new \Distance\Drivers\GeoDataSource, $position, $destination), 2)
 									. "km, ";

 			}

 		} catch (\Exception $e) {

 		}

 	}

	return $nameOfHospitals;

 }

if( ! isset($_GET['file'])) exit('Filename is missing');

if( ! isset($_GET['device'])) exit('Device parameter is missing');

if( ! $file = @file_get_contents('logs/' . $_GET['device'] . '/' . $_GET['file'] . '.txt')) exit('File is missing');

$file = unserialize($file);

foreach ($file['events'] as $key => $value) {	

	try {

		$parameters = Parser::parseBTS($value['value']);

		$coordinates = LocatorFactory::find(new GoogleService, $parameters);



	} catch(\Exception $e) {

		$coordinates['lon'] = '-';

		$coordinates['lat'] = '-';

	}


	$file['events'][$key]['longitude'] 	= $coordinates['lon'];

	$file['events'][$key]['latitude'] 	= $coordinates['lat'];

	if ( $coordinates['lon'] != '-' && $coordinates['lat'] != '-' ){

		$file['events'][$key]['close_to'] 	=  nameOfInRangeHospitals($file['events'][$key], $hospitals);

	}
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DEMO - Device parser</title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.js"></script>
	<script src="https://www.mapquestapi.com/sdk/leaflet/v2.2/mq-map.js?key=pk6eONr3zXpNfHdxAep8NcZB3yjUbr6n"></script>

    <!-- Custom CSS -->
    <link href="assets/css/shop-item.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
   <?php include('partials/navigation.php');?>
 
    <!-- Page Content -->
    <div class="container">
        <div class="row">
        <button @click="clear" class="btn btn-default">Clear</button>
		<label class="checkbox-inline"><input type="checkbox" @change="selectEvents" v-model="selectedEvents" value="LID-OP">Open</label>
		<label class="checkbox-inline"><input type="checkbox" @change="selectEvents" v-model="selectedEvents" value="LID-CS" ">Close</label>
		<label class="checkbox-inline"><input type="checkbox" @change="selectEvents" v-model="selectedEvents" value="BTS">Move</label>
			<template id="events-list">
				<ul v-for="ev in list" class="list-group"> 
					<li class="list-group-item">
						<span class="badge">{{ ev.time }}</span> Time:
					</li>
					<li class="list-group-item">
						<span class="badge">{{ ev.event }}</span> Event:
					</li>
					<li class="list-group-item">
						<span class="badge">{{ ev.value }}</span> Value:
					</li>
					<li class="list-group-item">
						<span class="badge">{{ ev.longitude }}</span> Longitude:
					</li>
					<li class="list-group-item">
						<span class="badge">{{ ev.latitude }}</span> Latitude:
					</li>
					<li class="list-group-item">
						<span class="badge">{{ ev.close_to }}</span> Close to:
					</li>
				</ul>
				<span v-show=" ! list.length">No events selected</span>	
			</template>


            <div class="col-md-3">
            	<ul class="pager">
                    <li class="previous"><a href="list.php?device={{device}}"><< Back to all requests</a></li>
                </ul>	
				
				<hr>
                <p class="lead">Request contents:</p>
                <h5><strong>Details:</strong></h5>	
                <br>
                <ul class="list-group"> 	
                	<li class="list-group-item"><strong>IN : </strong> <span>{{ header.in }}</span></li>
                	<li class="list-group-item"><strong>CATOS : </strong> <span>{{ header.catos }}</span></li>
                	<li class="list-group-item"><strong>DT : </strong> <span>{{ header.dt }}</span></li>
                	<li class="list-group-item"><strong class="inline middle">BAT : </strong>
					<div class="progress middle" style="width:calc(100% - 45px); display:inline-block; margin-bottom:0;">
						<div class="progress-bar progress-bar-{{bateryClass}}" role="progressbar" aria-valuenow="{{ header.bat }}"
						aria-valuemin="0" aria-valuemax="100" style="width:{{ header.bat }}%">
						{{ header.bat }}
						</div>
					</div>
					</li>
                </ul>
                <hr>

				<h5><strong>Events: <span>({{ evts.length }})</span></strong></h5>
				<br>
                <div class="scroll"> 	
                	<eventing :list=evts></eventing>  		
                </div>
            </div>

            <div class="col-md-9">

				<h1>Map of events:</h1>
				<br>
                <div class="well">
					<div id="myMap" style="height: 700px; width: 100%;"></div>

                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <?php include('partials/footer.php');?>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>

	 <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script>
	 <script>
	 var events 	= <?php echo json_encode($file['events']);?>;
	 var header 	= <?php echo json_encode($file['header']);?>;
	 var device 	= <?php echo json_encode($_GET['device']);?>;
	 var hospitals 	= <?php echo json_encode($hospitals);?>;

	 Vue.component('eventing', {
	 	template: '#events-list',
	 	props: ['list']
	 });
	 	
	 new Vue({
	 	el: 'body',
	 	computed: {
	 		bateryClass: function() {
	 			if(this.header.bat >= 75) return 'success';
    			else if(this.header.bat >= 30) return 'warning';
    			else return 'danger';
	 		}
	 	},
	 	data: {
	 		validEvents: ['LID-OP', 'LID-CS', 'BTS'],
	 		selectedEvents: ['LID-OP', 'LID-CS', 'BTS'],
	 		map: false,
	 		mapLayer: false,
	 		mapMarkers: [],
	 		device: device,
	 		header: header,
	 		evts: events,
	 		originalEvents: events,
	 		hospitals: hospitals
	 	},
	 	methods: {
	 		addEventsToMap: function(events) {

	 			if( ! events.length) return;

	 			for(i in events) {

	 				if (isNaN(events[i].latitude) || isNaN(events[i].longitude)) continue;

	 				var marker = L.marker([events[i].latitude, events[i].longitude], {
								riseOnHover: true
							});

					marker.addTo(this.map);

					marker.bindPopup("<b>Event: " + events[i].event + "</b><br>" +
						"Value: " + events[i].value + "<br>" +
						"Time: " + events[i].time + "<br>" +
						"Latitude: " + events[i].latitude + "<br>" +
						"Longitude: " + events[i].longitude + "<br>" +
						"Close To: " + events[i].close_to + "<br>");

					this.mapMarkers.push(marker);


	 			}
	 		},

	 		addHospitalsToMap: function() {

	 			if( ! this.hospitals || ! this.hospitals.length) return;

	 			for (i in this.hospitals) {

	 				if (isNaN(this.hospitals[i].latitude) || isNaN(this.hospitals[i].longitude)) continue;
						

					var circle = L.circle([this.hospitals[i].latitude, this.hospitals[i].longitude], this.hospitals[i].radius, {
						color: 'red',
						fillColor: '#f03',
						fillOpacity: 0.5
					}).addTo(this.map);

					 circle.bindPopup("<b>Hospital: " + this.hospitals[i].name +" "+ this.hospitals[i].name_extended+" " + this.hospitals[i].clinic_nr+"</b><br>" +
						"Latitude: " + this.hospitals[i].latitude + "<br>" +
						"Longitude: " + this.hospitals[i].longitude + "<br>" +
						"Adress: " + this.hospitals[i].street + " " + this.hospitals[i].street_nr + "<br>" +
						"Place: " + this.hospitals[i].place + " " + this.hospitals[i].zip + "<br>"+
					 	"Radius: " + this.hospitals[i].radius +  " m<br>");

						
				}

	 		}, 

	 		getBoundsFromEvenst: function(){

				var myLocations = [];

				if( ! this.evts.length) return;

				for (i in this.evts) {

					if ( isNaN(this.evts[i].latitude) ||  isNaN(this.evts[i].longitude)) continue;

					myLocations.push([this.evts[i].latitude, this.evts[i].longitude]);
					
				}
				

				return myLocations;

			},

			filterEvents: function() {

				var _this = this;

				this.evts = this.originalEvents.filter(function(event) {
					return _this.selectedEvents.indexOf(event.event) !== -1;
				});

			},

			selectEvents: function() {

				this.filterEvents();

				this.clearMarkers();

				this.addEventsToMap(this.evts);
				
			},

			clearMarkers: function() {
				for(i in this.mapMarkers) {
					this.map.removeLayer(this.mapMarkers[i]);
				}
			},

			clear: function() {
				for(i in this.mapMarkers) {
					this.map.removeLayer(this.mapMarkers[i]);
				}

				this.evts = [];
				this.selectedEvents = [];
			},	

			initialize: function() {

				this.mapLayer = MQ.mapLayer();

		 		this.map = L.map('myMap', {
					layers: this.mapLayer,
				});

				this.map.fitBounds(this.getBoundsFromEvenst());

				L.control.layers({
					'Map': this.mapLayer,
					'Hybrid': MQ.hybridLayer(),
					'Satellite': MQ.satelliteLayer(),
					'Dark': MQ.darkLayer(),
					'Light': MQ.lightLayer()
				}).addTo(this.map);

				this.filterEvents();

				this.addEventsToMap(this.evts);

				this.addHospitalsToMap();

			}
	 	},
	 	ready() {

	 		this.initialize();	
	 		
	 	}
	 });
	 </script>

</body>

</html>
