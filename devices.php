<?php
	$dirs = array_diff(scandir('logs'), ['.', '..']);
?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DEMO - Device parser</title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/shop-item.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <?php include('partials/navigation.php');?>

    <?php
        $dirs = array_diff(scandir('logs'), ['.', '..']);
        array_filter($dirs, function($el) {
            return is_dir('logs/' . $el);
        });
    ?>

    
 
    <!-- Page Content -->
    <div class="container">

        
        

        <template id="device-list">
            <li v-for="device in list" class="m-b-5"> 
                <a href="list.php?device={{device}}" class="list-group-item active">
                    <strong>IMEI:</strong>   {{ device }}                           
                </a>
            </li>     
        </template>
           



        <div class="row">

            <div class="col-md-3">
                <p class="lead">All devices:</p>
                <div class="list-group">                    
                    <ol class="list-nav-box">                        
                        <devices list=<?php echo json_encode($dirs);?>></devices>             
                    </ol>                   
                </div>
            </div>

            <div class="col-md-9">

               
            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <?php include('partials/footer.php');?>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script>
    <script>


    Vue.component('devices', {

        template: '#device-list',

        props: ['list'],

        created() {
            this.list = JSON.parse(this.list);
        }
    });
      
    new Vue({
        el: 'body',        

    })
    </script>

</body>

</html>
