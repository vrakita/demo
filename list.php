<?php

require __DIR__ . '/vendor/autoload.php';

if( ! isset($_GET['device'])) exit('Device parameter is missing');


if( ! is_dir($path = 'logs/' . $_GET['device'])) exit('Log directory is empty');

$dir = scandir($path);

$list = [];

foreach($dir as $file) {
    
    if(strlen($file) <= 2) continue;
    
    $date = explode('.', $file);
    
    $list[] = [
        'filename'  => $date[0],
        'time'      => date('Y-m-d H:i:s', $date[0])
    ];    

   $list = json_decode(json_encode($list), FALSE);
    
}

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DEMO - Device parser</title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/shop-item.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
   <?php include('partials/navigation.php');?>
 
    <!-- Page Content -->
    <div class="container">

        <div class="row">                    

            <div class="col-md-3">
			
                <ul class="pager">
                    <li class="previous"><a href="devices.php"><< Back to all devices</a></li>
                </ul>				
				<hr>
                <p class="lead">Device: {{ device }}</p>
                <h5>Requests:</h5>
                <div class="list-group">
                    <ol class="list-nav-box">
                        <requests list='<?php echo json_encode($list, JSON_FORCE_OBJECT);?>'></requests>
                    </ol>        
                </div>
            </div>

            <div class="col-md-9">

            <template id="request-list">
                <li v-for="request in list">
                    <a href="read.php?file={{ request.filename}}&device={{ device }}" class="list-group-item">{{ request.time }}</a>
                </li>
            </template>

            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <?php include('partials/footer.php');?>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script>
    <script>

    var device = '<?php echo $_GET['device'];?>';

    Vue.component('requests', {

        template: '#request-list',

        props: ['list'],

        created() {
            this.list = JSON.parse(this.list);
            this.device = this.$parent.getDevice();
        }       
        
    });

    new Vue({
        el: 'body',
        data: {
            device: device
        },
        methods: {
            getDevice: function() {
                return this.device;
            }
        }
    });
    </script>

</body>

</html>
