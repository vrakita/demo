<?php

require __DIR__ . '/vendor/autoload.php';

use DeviceParser\Parser;

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    header('Location: devices.php');
    exit;
}

$fileName = time() . '.txt';

file_put_contents('logs_raw/' . $fileName, file_get_contents('php://input'), FILE_APPEND);

try {

	$parser = Parser::parse(file_get_contents('php://input'));


} catch(\Exception $e) {
	header("HTTP/1.1 400 OK");
	header('Content-Type: application/json');
	echo json_encode($e->getMessage());
	exit;
}

$filename = time() . 'txt';

if ( ! is_dir('logs/' . $parser->getHeader()['in'])) {

	mkdir('logs/' . $parser->getHeader()['in']);

}


file_put_contents('logs/' . $parser->getHeader()['in'] . '/' . $fileName, serialize(['header' => $parser->getHeader(), 'events' => $parser->getEvents()]), FILE_APPEND);


header("HTTP/1.1 200 OK");
header('Content-Type: application/json');
echo json_encode('Content successfully stored');